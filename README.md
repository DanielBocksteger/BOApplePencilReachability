# BOApplePencilReachability
I've developed the `BOApplePencilReachability` class to determine whether an Apple Pencil is currently connected and to get notified if there are changes to this state.

## Usage
To explain you, how this class is meant to be used the following examples show the call both in Swift and in Objective-C.

### Swift
```swift
private func setupPencilChecker() {
    pencilChecker = BOApplePencilReachability { (state) in
        if state == true {
            print("Pencil is available")
        } else {
            print("There's no pencil currently available")
        }
    }
}
```

### Objective-C
```cpp
- (void)setupPencilChecker {
    _pencilChecker = [[BOApplePencilReachability alloc] initWithDidChangeClosure:^void (BOOL state) {
        if (state == YES) {
            NSLog(@"Pencil is available");
        } else {
            NSLog(@"There's no pencil currently available");
        }
    }];
}
```
