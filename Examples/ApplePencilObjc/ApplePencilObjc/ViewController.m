//
//  ViewController.m
//  ApplePencilObjc
//
//  Created by Daniel Bocksteger on 15.10.17.
//  Copyright © 2017 Daniel Bocksteger. All rights reserved.
//

#import "ViewController.h"

#import "ApplePencilObjc-Swift.h"

@interface ViewController ()

@property (nonatomic, strong, nullable) BOApplePencilReachability *pencilChecker;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setupPencilChecker];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupPencilChecker {
    _pencilChecker = [[BOApplePencilReachability alloc] initWithDidChangeClosure:^void (BOOL state) {
        if (state == YES) {
            NSLog(@"Pencil is available");
        } else {
            NSLog(@"There's no pencil currently available");
        }
    }];
}

@end
