//
//  ViewController.swift
//  ApplePencilSwift
//
//  Created by Daniel Bocksteger on 15.10.17.
//  Copyright © 2017 Daniel Bocksteger. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private var pencilChecker: BOApplePencilReachability?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupPencilChecker()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupPencilChecker() {
        pencilChecker = BOApplePencilReachability { (state) in
            if state == true {
                print("Pencil is available")
            } else {
                print("There's no pencil currently available")
            }
        }
    }
}

